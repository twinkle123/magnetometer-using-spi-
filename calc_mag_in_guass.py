import sys

if len(sys.argv) != 5:
    print('Enter valid number of arguments: <sensitivity> <mag_x> <mag_y> <mag_z>')
    sys.exit(0)

sensitivity = float(sys.argv[1])
mag_x = sys.argv[2].replace(",", "")
mag_y = sys.argv[3].replace(",", "")
mag_z = sys.argv[4].replace(",", "")

if len(mag_x) != 6:
    temp = '0x'
    for i in range(6 - len(mag_x)):
        temp = temp + '0'
    mag_x = temp + mag_x[2:]

if len(mag_y) != 6:
    temp = '0x'
    for i in range(6 - len(mag_y)):
        temp = temp + '0'
    mag_y = temp + mag_y[2:]

if len(mag_z) != 6:
    temp = '0x'
    for i in range(6 - len(mag_z)):
        temp = temp + '0'
    mag_z = temp + mag_z[2:]

print(mag_x)
print(mag_y)
print(mag_z)

if len(mag_x) != 6 or len(mag_y) != 6 or len(mag_z) != 6:
    print('Enter the hex values in the following format: 0xhhhh')
    print('h: valid hex digit')
    sys.exit(0)

if mag_x[2] >= '8' and (mag_x[2] <= 'f' or mag_x[2] <= 'F'):
    mag_x = -((int(mag_x, 0) ^ 65535) + 1)
else:
    mag_x = int(mag_x, 0)
if mag_y[2] >= '8' and (mag_y[2] <= 'f' or mag_y[2] <= 'F'):
    mag_y = -((int(mag_y, 0) ^ 65535) + 1)
else:
    mag_y = int(mag_y, 0)
if mag_z[2] >= '8' and (mag_z[2] <= 'f' or mag_z[2] <= 'F'):
    mag_z = -((int(mag_z, 0) ^ 65535) + 1)
else:
    mag_z = int(mag_z, 0)

print('mag_x: ' + str(mag_x*sensitivity))
print('mag_y: ' + str(mag_y*sensitivity))
print('mag_z: ' + str(mag_z*sensitivity))

print('mag_total: ' + str((mag_x**2 + mag_y**2 + mag_z**2)**0.5*sensitivity))
