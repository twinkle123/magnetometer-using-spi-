#include "board.h"
#include <stdint.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>

/*----------------------------------------------------------------------------
 *        Local definitions
 *----------------------------------------------------------------------------*/
#define SPI0_CS0  0
#define SPI0_CS1  1
#define SPI0_CS2  2
#define SPI0_CS3  3


/** TWI clock frequency in Hz. */
#define TWCK            100000
#define spiClock	1000000
/** Slave address of twi_eeprom example.*/
#define AT24MAC_ADDRESS			0x57
#define AT24MAC_SERIAL_NUM_ADD		0x5F
#define Random_address			0x55
#define LSM_address			0x6a
#define ACT_THS				0x04
#define ACT_DUR				0x05
#define INT_GEN_CFG_XL			0x06
#define INT_GEN_THS_X_XL		0x07
#define INT_GEN_THS_Y_XL		0x08
#define INT_GEN_THS_Z_XL		0x09
#define INT_GEN_DUR_XL			0x0A
#define REFERENCE_G			0x0B
#define INT1_CTRL			0x0C
#define INT2_CTRL			0x0D
#define WHO_AM_I_XG			0x0F
#define CTRL_REG1_G			0x10
#define CTRL_REG2_G			0x11
#define CTRL_REG3_G			0x12
#define ORIENT_CFG_G			0x13
#define INT_GEN_SRC_G			0x14
#define OUT_TEMP_L			0x15
#define OUT_TEMP_H			0x16
#define STATUS_REG_0			0x17
#define OUT_X_L_G			0x18
#define OUT_X_H_G			0x19
#define OUT_Y_L_G			0x1A
#define OUT_Y_H_G			0x1B
#define OUT_Z_L_G			0x1C
#define OUT_Z_H_G			0x1D
#define CTRL_REG4			0x1E
#define CTRL_REG5_XL			0x1F
#define CTRL_REG6_XL			0x20
#define CTRL_REG7_XL			0x21
#define CTRL_REG8			0x22
#define CTRL_REG9			0x23
#define CTRL_REG10			0x24
#define INT_GEN_SRC_XL			0x26
#define STATUS_REG_1			0x27
#define OUT_X_L_XL			0x28
#define OUT_X_H_XL			0x29
#define OUT_Y_L_XL			0x2A
#define OUT_Y_H_XL			0x2B
#define OUT_Z_L_XL			0x2C
#define OUT_Z_H_XL			0x2D
#define FIFO_CTRL			0x2E
#define FIFO_SRC			0x2F
#define INT_GEN_CFG_G			0x30
#define INT_GEN_THS_XH_G		0x31
#define INT_GEN_THS_XL_G		0x32
#define INT_GEN_THS_YH_G		0x33
#define INT_GEN_THS_YL_G		0x34
#define INT_GEN_THS_ZH_G		0x35
#define INT_GEN_THS_ZL_G		0x36
#define INT_GEN_DUR_G			0x37

/** Page numbers of an AT24MAC402 chip */
#define EEPROM_PAGES    16

/** Page size of an AT24MAC402 chip (in bytes)*/
#define PAGE_SIZE       16

/** EEPROM Pins definition */
#define BOARD_PINS_TWI_EEPROM PINS_TWI0

/** TWI0 peripheral ID for EEPROM device*/
#define BOARD_ID_TWI_EEPROM   ID_TWIHS0

/** TWI0 base address for EEPROM device */
#define BOARD_BASE_TWI_EEPROM TWIHS0


/*----------------------------------------------------------------------------
 *        Local definitions
 *----------------------------------------------------------------------------*/

/** IRQ priority for PIO (The lower the value, the greater the priority) */
#define IRQ_PRIOR_PIO    0

/** LED0 blink time, LED1 blink half this time, in ms */
#define BLINK_PERIOD        1000

/*----------------------------------------------------------------------------
 *        Local variables
 *----------------------------------------------------------------------------*/

/** LED0 blinking control. */
volatile bool bLed0Active = true;

/** LED1 blinking control. */
volatile bool bLed1Active = true;

/** Global timestamps in milliseconds since start of application */
volatile uint32_t dwTimeStamp = 0;

/** Global timestamps in milliseconds since start of application */
volatile uint32_t dwTcCounter = 0;

/*----------------------------------------------------------------------------
 *        Local functions
 *----------------------------------------------------------------------------*/

/**
 *  \brief Process Buttons Events
 *
 *  Change active states of LEDs when corresponding button events happened.
 */
static void ProcessButtonEvt(uint8_t ucButton)
{
	if (ucButton == 0) {
		bLed0Active = !bLed0Active;
		if (!bLed0Active) {
			LED_Clear(0);
		}
	} else {
		bLed1Active = !bLed1Active;
#if 2 == LED_NUM
		/* Enable LED#2 and TC if they were disabled */
		if (bLed1Active) {
			LED_Set(1);
		}
		/* Disable LED#2 and TC if they were enabled */
		else {
			LED_Clear(1);
		}
#endif
	}
}

#ifndef NO_PUSHBUTTON
/**
 *  \brief Handler for Button 1 rising edge interrupt.
 *
 *  Handle process led1 status change.
 */
static void _Button1_Handler(const Pin* pPin)
{
	if (pPin == &pinPB1) {
		ProcessButtonEvt(0);
	}
}

/**
 *  \brief Handler for Button 2 falling edge interrupt.
 *
 *  Handle process led2 status change.
 */
static void _Button2_Handler(const Pin* pPin)
{
	if (pPin == &pinPB2) {
		ProcessButtonEvt(1);
	}
}

/**
 *  \brief Configure the Push-buttons
 *
 *  Configure the PIO as inputs and generate corresponding interrupt when
 *  pressed or released.
 */
static void _ConfigureButtons(void)
{
	/* Configure PIO as inputs. */
	PIO_Configure(&pinPB1, 1);
	PIO_Configure(&pinPB2, 1);

	/* Adjust PIO denounce filter parameters, uses 10 Hz filter. */
	PIO_SetDebounceFilter(&pinPB1, 10);
	PIO_SetDebounceFilter(&pinPB2, 10);

	/* Initialize PIO interrupt handlers, see PIO definition in board.h. */
	PIO_ConfigureIt(&pinPB1, _Button1_Handler); /* Interrupt on rising edge  */
	PIO_ConfigureIt(&pinPB2, _Button2_Handler); /* Interrupt on rising edge */

	/* Enable PIO controller IRQs. */
	NVIC_EnableIRQ((IRQn_Type)pinPB1.id);
	NVIC_EnableIRQ((IRQn_Type)pinPB2.id);

	/* Enable PIO line interrupts. */
	PIO_EnableIt(&pinPB1);
	PIO_EnableIt(&pinPB2);
}

#else

/**
 *  \brief Handler for DBGU input.
 *
 *  Handle process LED1 or LED2 status change.
 */
static void _DBGU_Handler(void)
{
	uint8_t key;

	if (!DBG_IsRxReady())
		return;
	key = DBG_GetChar();
	switch (key) {
		case '1':
		case '2':
			ProcessButtonEvt(key - '1');
			break;
	}
}
#endif
/**
 *  \brief Configure LEDs
 *
 *  Configures LEDs \#1 and \#2 (cleared by default).
 */
static void _ConfigureLeds(void)
{
	uint32_t i;

	for(i = 0; i < LED_NUM; i++)
		LED_Configure(i);
}


/**
 *  Interrupt handler for TC0 interrupt. Toggles the state of LED\#2.
 */
void TC0_Handler(void)
{
	volatile uint32_t dummy;

	/* Clear status bit to acknowledge interrupt */
	dummy = TC0->TC_CHANNEL[0].TC_SR;

	/** Toggle LED state. */
	if (bLed1Active) {
#if 2 == LED_NUM
		LED_Toggle(1);
#endif
		printf("2 ");
	}
#ifdef NO_PUSHBUTTON
	_DBGU_Handler();
#endif

}

/**
 *  Configure Timer Counter 0 to generate an interrupt every 250ms.
 */
static void _ConfigureTc(void)
{
	uint32_t div;
	uint32_t tcclks;

	/** Enable peripheral clock. */
	PMC_EnablePeripheral(ID_TC0);
	/** Configure TC for a 4Hz frequency and trigger on RC compare. */
	TC_FindMckDivisor(4, BOARD_MCK, &div, &tcclks, BOARD_MCK);

	TC_Configure(TC0, 0, tcclks | TC_CMR_CPCTRG);
	TC0->TC_CHANNEL[0].TC_RC = (BOARD_MCK / div) / 4;

	/* Configure and enable interrupt on RC compare */
	NVIC_ClearPendingIRQ(TC0_IRQn);
	NVIC_EnableIRQ(TC0_IRQn);

	TC0->TC_CHANNEL[0].TC_IER = TC_IER_CPCS;

	/** Start the counter if LED1 is enabled. */
	if (bLed1Active) {
		TC_Start(TC0, 0);
	}
}

// Local variables for timer functions
/*----------------------------------------------------------------------------
 *        Local definitions
 *----------------------------------------------------------------------------*/
#define PIN_TC_TIOA_OUT   {PIO_PA0B_TIOA0, PIOA, ID_PIOA, PIO_PERIPH_B, PIO_DEFAULT}
#define PIN_TC_TIOA_IN    {PIO_PD21C_TIOA11, PIOD, ID_PIOD, PIO_PERIPH_C, PIO_DEFAULT}

#define TC_WAVE_BASE       TC0
#define TC_WAVE_ID         ID_TC0
#define TC_WAVE_CHANNEL    0

#define TC_CAPTURE_BASE    TC3
#define TC_CAPTURE_ID      ID_TC11
#define TC_CAPTURE_CHANNEL 2
#define TC_Handler         TC11_Handler
#define TC_IRQn            TC11_IRQn
/*----------------------------------------------------------------------------
 *        Types
 *----------------------------------------------------------------------------*/
/** Describes a possible Timer configuration as waveform mode */
struct WaveformConfiguration {
	/** Internal clock signals selection. */
	uint32_t clockSelection;
	/** Waveform frequency (in Hz). */
	uint16_t frequency;
	/** Duty cycle in percent (positive)*/
	uint16_t dutyCycle;
};

/*----------------------------------------------------------------------------
 *        Local variables
 *----------------------------------------------------------------------------*/

/** PIOs for TC0 */
static const Pin pTcPins[] = {PIN_TC_TIOA_OUT, PIN_TC_TIOA_IN};

static const struct WaveformConfiguration waveformConfigurations[] = {

	{TC_CMR_TCCLKS_TIMER_CLOCK4, 400, 30},
	{TC_CMR_TCCLKS_TIMER_CLOCK3, 500, 50},
	{TC_CMR_TCCLKS_TIMER_CLOCK3, 800, 75},
	{TC_CMR_TCCLKS_TIMER_CLOCK2, 1000, 80},
	{TC_CMR_TCCLKS_TIMER_CLOCK2, 4000, 55}
};

/** Current wave configuration*/
static uint8_t configuration = 0;

/** Number of available wave configurations */
const uint8_t numConfigurations = sizeof(waveformConfigurations) /
		sizeof(struct WaveformConfiguration);
/** Capture status*/
static uint32_t _dwCaptured_pulses;
static uint32_t _dwCaptured_ra;
static uint32_t _dwCaptured_rb;
const uint32_t divisors[5] = {2, 8, 32, 128, BOARD_MCK / 32768};





/*----------------------------------------------------------------------------
 *        Local variables
 *----------------------------------------------------------------------------*/
/** PIO pins to configure. */
static const Pin pins[] = {PIN_SPI_MISO, PIN_SPI_MOSI, PIN_SPI_SPCK, PIN_SPI_NPCS3};
static TwihsDma twi_dma;

/** TWI driver instance.*/
static Twid twid;

/** Page buffer.*/
COMPILER_ALIGNED(32) static uint8_t pData[PAGE_SIZE];

static uint8_t CallBackFired = 0;

void TWIHS0_Handler(void)
{
	TWID_Handler(&twid);
}

void XDMAC_Handler(void)
{
	XDMAD_Handler(twi_dma.pTwiDma);
}

static void TestCallback(void)
{
	CallBackFired++;
}

static void _fillBuffer(uint8_t *pbuff)
{
	uint32_t i;

	/* Write checkerboard pattern in first page */
	for (i = 0; i < PAGE_SIZE; i++) {
		/* Even*/
		if ((i & 1) == 0)
			pbuff[i] = 0xA5;
		/* Odd */
		else
			pbuff[i] = 0x5A;
	}
}

static void _checkReadBuffer(uint8_t *pBuff)
{
	uint16_t i, NoError;

	NoError = 0;
	for (i = 0; i < PAGE_SIZE; i++) {
		/* Even */
		if (((i & 1) == 0) && (pBuff[i] != 0xA5)) {
			printf( "-E- Data mismatch at offset #%u: expected 0xA5, \
					read 0x%02x\n\r", (unsigned int)i, (unsigned int)pData[i] );
			NoError++;
		}
		/* Odd */
		else {
			if (((i & 1) == 1) && (pBuff[i] != 0x5A)) {
				printf( "-E- Data mismatch at offset #%u: expected 0x5A, read\
						0x%02x\n\r", (unsigned int)i, (unsigned int)pData[i]);
				NoError++;
			}
		}
	}
	printf("-I- %u comparison error(s) found \r", (unsigned int)NoError);
}


/*----------------------------------------------------------------------------
 *        Local functions
 *----------------------------------------------------------------------------*/

/**
 * \brief Interrupt handler for the TC capture.
 */
void TC_Handler(void)
{
	uint32_t status;
	status = TC_CAPTURE_BASE->TC_CHANNEL[TC_CAPTURE_CHANNEL].TC_SR;

	if ((status & TC_SR_LDRBS) == TC_SR_LDRBS) {
		_dwCaptured_pulses++;
		_dwCaptured_ra = TC_CAPTURE_BASE->TC_CHANNEL[TC_CAPTURE_CHANNEL].TC_RA;
		_dwCaptured_rb = TC_CAPTURE_BASE->TC_CHANNEL[TC_CAPTURE_CHANNEL].TC_RB;
	}
}

/**
 * \brief Configure clock, frequency and dutycycle for TC0 channel 1 in waveform mode.
 */
static void TcWaveformConfigure(void)
{
	uint32_t ra, rc;

	/*  Set channel 1 as waveform mode*/
	TC_WAVE_BASE->TC_CHANNEL[TC_WAVE_CHANNEL].TC_CMR =
		waveformConfigurations[configuration].clockSelection/* Waveform Clock Selection */
		| TC_CMR_WAVE                                    /* Waveform mode is enabled */
		| TC_CMR_ACPA_SET                                /* RA Compare Effect: set */
		| TC_CMR_ACPC_CLEAR                              /* RC Compare Effect: clear */
		| TC_CMR_CPCTRG;                                 /* UP mode with automatic trigger on RC Compare */
	rc = (BOARD_MCK / divisors[waveformConfigurations[configuration].clockSelection]) \
		 / waveformConfigurations[configuration].frequency;
	TC_WAVE_BASE->TC_CHANNEL[TC_WAVE_CHANNEL].TC_RC = rc;
	ra = (100 - waveformConfigurations[configuration].dutyCycle) * rc / 100;
	TC_WAVE_BASE->TC_CHANNEL[TC_WAVE_CHANNEL].TC_RA = ra;
}

/**
 * \brief Configure TC with waveform operating mode.
 */
static void TcWaveformInitialize(void)
{
	/* Configure the PMC to enable the Timer Counter clock for TC wave */
	PMC_EnablePeripheral(TC_WAVE_ID);
	/*  Disable TC clock */
	TC_WAVE_BASE->TC_CHANNEL[TC_WAVE_CHANNEL].TC_CCR = TC_CCR_CLKDIS;
	/*  Disable interrupts */
	TC_WAVE_BASE->TC_CHANNEL[TC_WAVE_CHANNEL].TC_IDR = 0xFFFFFFFF;
	/*  Clear status register */
	TC_WAVE_BASE->TC_CHANNEL[TC_WAVE_CHANNEL].TC_SR;
	/* Configure waveform frequency and duty cycle */
	TcWaveformConfigure();
	/* Enable TC0 channel 0 */
	TC_WAVE_BASE->TC_CHANNEL[TC_WAVE_CHANNEL].TC_CCR =  TC_CCR_CLKEN | TC_CCR_SWTRG;
	printf ("Start waveform: Frequency = %d Hz,Duty Cycle = %2d%%\n\r",
			waveformConfigurations[configuration].frequency,
			waveformConfigurations[configuration].dutyCycle);
}

/**
 * \brief Configure TC with capture operating mode.
 */
static void TcCaptureInitialize(void)
{
	/* Configure the PMC to enable the Timer Counter clock */
	PMC_EnablePeripheral(TC_CAPTURE_ID);
	/*  Disable TC clock */
	TC_CAPTURE_BASE->TC_CHANNEL[TC_CAPTURE_CHANNEL].TC_CCR = TC_CCR_CLKDIS;
	/*  Disable interrupts */
	TC_CAPTURE_BASE->TC_CHANNEL[TC_CAPTURE_CHANNEL].TC_IDR = 0xFFFFFFFF;
	/*  Clear status register */
	TC_CAPTURE_BASE->TC_CHANNEL[TC_CAPTURE_CHANNEL].TC_SR;
	/*  Set channel 2 as capture mode */
	TC_CAPTURE_BASE->TC_CHANNEL[TC_CAPTURE_CHANNEL].TC_CMR = \
		( TC_CMR_TCCLKS_TIMER_CLOCK2 /* Clock Selection */
		| TC_CMR_LDRA_RISING        /* RA Loading Selection: rising edge of TIOA */
		| TC_CMR_LDRB_FALLING       /* RB Loading Selection: falling edge of TIOA */
		| TC_CMR_ABETRG            /* External Trigger Selection: TIOA */
		| TC_CMR_ETRGEDG_FALLING    /* External Trigger Edge Selection: Falling edge */
		);
}



void read_LSM(uint16_t *x, uint16_t *y) {
	uint8_t *x1axis = 0, *x2axis = 0, *y1axis = 0, *y2axis = 0;
	TWID_Read(&twid, LSM_address, 0x18, 1, x1axis, 1, 0);
	TWID_Read(&twid, LSM_address, 0x19, 1, x2axis, 1, 0);
	TWID_Read(&twid, LSM_address, 0x1A, 1, y1axis, 1, 0);
	TWID_Read(&twid, LSM_address, 0x1B, 1, y2axis, 1, 0);
	*x = (*x2axis << 8) | *x1axis;
	*y = (*y2axis << 8) | *y1axis;
}


//			/* Wait before sending the next byte */
//			startTime = GetTicks();
//
//			while (!TWI_ByteSent(pTwi)) {
//				if ((GetDelayInTicks(startTime, GetTicks())) > TWITIMEOUTMAX) {
//					TRACE_ERROR("TWID Timeout BS\n\r");
//					break;
//				}
//			}
//





extern int main( void )
{
	uint32_t dwCnt;
	uint8_t  ucError;
	uint32_t adwBuffer[IFLASH_PAGE_SIZE / 4];
	uint32_t dwLastPageAddress;
	volatile uint32_t *pdwLastPageData;

	uint8_t i, data = 0;
	uint16_t x = 0, y = 0, z = 0;
	uint8_t SNo[16];
	uint8_t q = 192, a = 0, b = 0, c = 0, d = 0, e = 0, f = 0;;
	Async async;
	/* Disable watchdog */
	WDT_Disable(WDT);
	/* Enable I and D cache */
	SCB_EnableICache();
	SCB_EnableDCache();

	CallBackFired = 0;
	TimeTick_Configure();

	/* Update internal flash Region to Full Access*/
	MPU_UpdateRegions(MPU_DEFAULT_IFLASH_REGION, IFLASH_START_ADDRESS, \
			MPU_AP_FULL_ACCESS |
			INNER_NORMAL_WB_NWA_TYPE( NON_SHAREABLE ) |
			MPU_CalMPURegionSize(IFLASH_END_ADDRESS - IFLASH_START_ADDRESS) |
			MPU_REGION_ENABLE);

	_ConfigureLeds();
	//_ConfigureTc();

	/* Set 6 WS for internal Flash writing (refer to errata) */
	EFC_SetWaitState(EFC, 6);

	/* Output example information */
	printf("\n\r\n\r\n\r");
	printf("EEFC Programming Example %s --\n\r", SOFTPACK_VERSION);
	printf("%s\n\r", BOARD_NAME);
	printf("-- Compiled: %s %s With %s--\n\r", __DATE__, __TIME__, COMPILER_NAME);

	/* Initialize flash driver */
	FLASHD_Initialize(BOARD_MCK, 0);

	/* Performs tests on last page (to avoid overriding existing program).*/
	dwLastPageAddress = IFLASH_ADDR + IFLASH_SIZE - IFLASH_PAGE_SIZE;
	pdwLastPageData = (volatile uint32_t *)dwLastPageAddress;
    if(*pdwLastPageData != 0xFFFFFFFF){
         while(1);
    }

	/* Unlock page */
	printf("-I- Unlocking last page......IN CHIRAG CODE\n\r");
	ucError = FLASHD_Unlock(dwLastPageAddress, dwLastPageAddress + IFLASH_PAGE_SIZE,
			0, 0);
	assert( !ucError );

	//
	//	uint32_t chirag = 0;
	//	/* Write page with walking bit pattern (0x00000001, 0x00000002, ...) */
	//	printf("-I- Writing last page with walking bit pattern\n\r");
	//	for (dwCnt = 0; dwCnt < (IFLASH_PAGE_SIZE / 4); dwCnt++){
	//		adwBuffer[dwCnt] = chirag++;
	//		//adwBuffer[dwCnt] = 1 << (dwCnt % 32);
	//	}
	//	ucError = FLASHD_Write(dwLastPageAddress, adwBuffer, IFLASH_PAGE_SIZE);
	//	assert(!ucError);
	//
	//
	//	FLASHD_EraseSector(dwLastPageAddress);
	//
	//	for (dwCnt = 0; dwCnt < (IFLASH_PAGE_SIZE / 4); dwCnt++){
	//		adwBuffer[dwCnt] = chirag--;
	//		//adwBuffer[dwCnt] = 1 << (dwCnt % 32);
	//	}
	//	ucError = FLASHD_Write(dwLastPageAddress, adwBuffer, IFLASH_PAGE_SIZE);
	//	assert(!ucError);
	//
	//	return 0;
	/* Lock page */
	//printf("-I- Locking last page\n\r");
	//ucError = FLASHD_Lock(dwLastPageAddress, dwLastPageAddress + IFLASH_PAGE_SIZE,
	//			0, 0 );
	//	assert(!ucError);

	/* Check that associated region is locked*/
	//	printf("-I- Try to program the locked page... \n\r");
	//	ucError = FLASHD_Write(dwLastPageAddress, adwBuffer, IFLASH_PAGE_SIZE);
	//	if (ucError)
	//		printf("-I- The page to be programmed belongs to a locked region.\n\r");
	//
	//	printf("-I- Please open Segger's JMem program \n\r");
	//	printf("-I- Read memory at address 0x%08x to check contents\n\r",
	//			(unsigned int)dwLastPageAddress);
	//printf("-I- Press any key to continue...\n\r");
	//	while ( !DBG_GetChar());

	//printf("-I- Good job!\n\r");
	//printf("-I- Now set the security bit \n\r");
	//printf("-I- Press any key to continue to see what happened...\n\r");
	//	while (!DBG_GetChar());

	//printf("-I- All tests done\n\r");

	/* Update internal flash Region to previous configurations */
	//	MPU_UpdateRegions(MPU_DEFAULT_IFLASH_REGION, IFLASH_START_ADDRESS, \
	//		MPU_AP_READONLY |
	//		INNER_NORMAL_WB_NWA_TYPE( NON_SHAREABLE ) |
	//		MPU_CalMPURegionSize(IFLASH_END_ADDRESS - IFLASH_START_ADDRESS) |
	//		MPU_REGION_ENABLE);
	//


	/* Configure SPI pins. */
	printf("configuring SPI pins\n\r");
	PIO_Configure(pins, PIO_LISTSIZE(pins));
        printf( "\n\r-I- Configure SPI master\n\r" );
        SPI_Configure(SPI0, ID_SPI0, (SPI_MR_MSTR | SPI_MR_MODFDIS
                                      | SPI_PCS( SPI0_CS3 )));
        SPI_ConfigureNPCS( SPI0,
                        SPI0_CS3,
                        SPI_DLYBCT( 1000, BOARD_MCK ) |
                        SPI_DLYBS(1000, BOARD_MCK) |
			SPI_CSR_CPOL | SPI_CSR_BITS_16_BIT| 
			SPI_SCBR( spiClock, BOARD_MCK) );

        /* Configure and enable interrupt on RC compare */
        NVIC_ClearPendingIRQ(SPI0_IRQn);
        NVIC_SetPriority(SPI0_IRQn ,1);
        //NVIC_EnableIRQ(SPI0_IRQn);

        //SPI_EnableIt(SPI0, SPI_IER_RDRF);
        SPI_Enable(SPI0);


	adwBuffer[255] = 0xffff;
	uint8_t w_data;
	SPI_Write(SPI0,SPI0_CS3,0x2160);
	SPI_Write(SPI0,SPI0_CS3,0x2280);
	SPI_Write(SPI0,SPI0_CS3,0x8f00);
	w_data = SPI_Read(SPI0);
	printf(" who_im_i data = %x\n\r",w_data);
	
	/*Configuring CTRL_REG values */
	

	//	count is the number which dictates 512 * count bytes
	//	count = 32 means last 16KB of flash written with data
	//	Approximately considering that code in flash takes 30 sectors, we start writing uptill 31st sector, which gives us
	//	96 sectors to write. Each sector is made up of 16KB. That gives us around 1572864 bytes to use.
	//	This gives us 262144 data sets of x, y, z.
	//	Also, remember that flash only writes 32bit data and not 8bit or 16bit. So convert smaller data
	//	accordingly.
	//	dwCnt refers to number of 2byte data points, and count refers to number of pages to used/written/read
	int sector = 50;
    	int pages_written = 0;
   	int sector_written = 0;
	int count = sector * 32;
   	for(dwCnt = 0; ;){
		Wait(500);
		if(dwCnt == ((IFLASH_PAGE_SIZE / 4) / 3) * 3) {
            dwCnt = 0;

			//FLASHD_EraseSector(dwLastPageAddress);
			ucError = FLASHD_Write(dwLastPageAddress, adwBuffer, IFLASH_PAGE_SIZE);
			assert(!ucError);
			//Wait(1000);
            pages_written++;
            if(pages_written % 32 == 0){
                printf("Sector #%d\n\r", ++sector_written);
            }
			dwLastPageAddress = dwLastPageAddress - IFLASH_PAGE_SIZE;
			count--;
			if(count == 0) {
				printf("--------------------- %d -------- %d\n\r", count, dwCnt);
				break;
			}
		}
		
		SPI_Write(SPI0, SPI0_CS3,0x8f00);
		w_data = SPI_Read(SPI0);
		if(w_data == 0x3d)
			LED_Set(0);
		SPI_Write(SPI0,SPI0_CS3,0X2000);
		printf("CTRL_REG1_M = %x\n\r",SPI_Read(SPI0));

		SPI_Write(SPI0,SPI0_CS3,0X2100);
                printf("CTRL_REG2_M = %x\n\r",SPI_Read(SPI0));

		SPI_Write(SPI0,SPI0_CS3,0X2200);
                printf("CTRL_REG3_M = %x\n\r",SPI_Read(SPI0));

		/*a = data;
		TWID_Read(&twid, LSM_address, 0x19, 1, &data, 1, 0);
		b = data;

		TWID_Read(&twid, LSM_address, 0x1A, 1, &data, 1, 0);
		c = data;
		TWID_Read(&twid, LSM_address, 0x1B, 1, &data, 1, 0);
		d = data;

		TWID_Read(&twid, LSM_address, 0x1C, 1, &data, 1, 0);
		e = data;
		TWID_Read(&twid, LSM_address, 0x1D, 1, &data, 1, 0);
		f = data;

			x = (b << 8) | a;
			adwBuffer[dwCnt++] = (uint32_t) x;
			y = (d << 8) | c;
			adwBuffer[dwCnt++] = (uint32_t) y;
			z = (f << 8) | e;
			adwBuffer[dwCnt++] = (uint32_t) z;
			printf("x = %f\t y = %f\t z = %f\n\r", x, y, z);
			//printf("x = %f\t y = %f\t z = %f\n\r", x * scale, y * scale, z * scale);
		//	printf("x = %d\t y = %d\t z = %d\t count = %d\n\r", x, y, z, dwCnt);

			//printf("x = %d   y = %d\n", x, y);

		*/
		Wait(1000);


		

	}

	LED_Clear(0);
	printf("Loop Ended\n\r");
	return 0;
}
