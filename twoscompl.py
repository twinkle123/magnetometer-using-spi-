import sys

mag_x = sys.argv[1]
mag_y = sys.argv[2]
mag_z = sys.argv[3]
sensitivity = 0.00014

if len(mag_x) != 6 or len(mag_y) != 6 or len(mag_z) != 6:
    print('Enter the hex values in the following format: 0xhhhh')
    print('h: valid hex digit')
    sys.exit(0)

if mag_x[2] >= '8' and (mag_x[2] <= 'f' or mag_x[2] <= 'F'):
    mag_x = -((int(mag_x, 0) ^ 65535) + 1)
else:
    mag_x = int(mag_x, 0)
if mag_y[2] >= '8' and (mag_y[2] <= 'f' or mag_y[2] <= 'F'):
    mag_y = -((int(mag_y, 0) ^ 65535) + 1)
else:
    mag_y = int(mag_y, 0)
if mag_z[2] >= '8' and (mag_z[2] <= 'f' or mag_z[2] <= 'F'):
    mag_z = -((int(mag_z, 0) ^ 65535) + 1)
else:
    mag_z = int(mag_z, 0)

print('mag_x: ' + str(mag_x));
print('mag_y: ' + str(mag_y));
print('mag_z: ' + str(mag_z));
