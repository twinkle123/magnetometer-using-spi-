#include <stdint.h>
#include "board.h"
#include <stdio.h>
#include <stdarg.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>
#include "LSM9DS1_Registers.h"

/*----------------------------------------------------------------------------
 *        Local definitions
 *----------------------------------------------------------------------------*/
#define SPI0_CS0  0
#define SPI0_CS1  1
#define SPI0_CS2  2
#define SPI0_CS3  3


/** TWI clock frequency in Hz. */
#define TWCK            100000
#define spiClock	4000000
/** Slave address of twi_eeprom example.*/
#define AT24MAC_ADDRESS			0x57
#define AT24MAC_SERIAL_NUM_ADD		0x5F
#define Random_address			0x55
#define LSM_address			0x6a
#define ACT_THS				0x04
#define ACT_DUR				0x05
#define INT_GEN_CFG_XL			0x06
#define INT_GEN_THS_X_XL		0x07
#define INT_GEN_THS_Y_XL		0x08
#define INT_GEN_THS_Z_XL		0x09
#define INT_GEN_DUR_XL			0x0A
#define REFERENCE_G			0x0B
#define INT1_CTRL			0x0C
#define INT2_CTRL			0x0D
#define WHO_AM_I_XG			0x0F
#define CTRL_REG1_G			0x10
#define CTRL_REG2_G			0x11
#define CTRL_REG3_G			0x12
#define ORIENT_CFG_G			0x13
#define INT_GEN_SRC_G			0x14
#define OUT_TEMP_L			0x15
#define OUT_TEMP_H			0x16
#define STATUS_REG_0			0x17
#define OUT_X_L_G			0x18
#define OUT_X_H_G			0x19
#define OUT_Y_L_G			0x1A
#define OUT_Y_H_G			0x1B
#define OUT_Z_L_G			0x1C
#define OUT_Z_H_G			0x1D
#define CTRL_REG4			0x1E
#define CTRL_REG5_XL			0x1F
#define CTRL_REG6_XL			0x20
#define CTRL_REG7_XL			0x21
#define CTRL_REG8			0x22
#define CTRL_REG9			0x23
#define CTRL_REG10			0x24
#define INT_GEN_SRC_XL			0x26
#define STATUS_REG_1			0x27
#define OUT_X_L_XL			0x28
#define OUT_X_H_XL			0x29
#define OUT_Y_L_XL			0x2A
#define OUT_Y_H_XL			0x2B
#define OUT_Z_L_XL			0x2C
#define OUT_Z_H_XL			0x2D
#define FIFO_CTRL			0x2E
#define FIFO_SRC			0x2F
#define INT_GEN_CFG_G			0x30
#define INT_GEN_THS_XH_G		0x31
#define INT_GEN_THS_XL_G		0x32
#define INT_GEN_THS_YH_G		0x33
#define INT_GEN_THS_YL_G		0x34
#define INT_GEN_THS_ZH_G		0x35
#define INT_GEN_THS_ZL_G		0x36
#define INT_GEN_DUR_G			0x37

/** Page numbers of an AT24MAC402 chip */
#define EEPROM_PAGES    16

/** Page size of an AT24MAC402 chip (in bytes)*/
#define PAGE_SIZE       16

/** EEPROM Pins definition */
#define BOARD_PINS_TWI_EEPROM PINS_TWI0

/** TWI0 peripheral ID for EEPROM device*/
#define BOARD_ID_TWI_EEPROM   ID_TWIHS0

/** TWI0 base address for EEPROM device */
#define BOARD_BASE_TWI_EEPROM TWIHS0


/*----------------------------------------------------------------------------
 *        Local definitions
 *----------------------------------------------------------------------------*/

/** IRQ priority for PIO (The lower the value, the greater the priority) */
#define IRQ_PRIOR_PIO    0

/** LED0 blink time, LED1 blink half this time, in ms */
#define BLINK_PERIOD        1000

/*----------------------------------------------------------------------------
 *        Local variables
 *----------------------------------------------------------------------------*/

/** LED0 blinking control. */
volatile bool bLed0Active = true;

/** LED1 blinking control. */
volatile bool bLed1Active = true;

/** Global timestamps in milliseconds since start of application */
volatile uint32_t dwTimeStamp = 0;

/** Global timestamps in milliseconds since start of application */
volatile uint32_t dwTcCounter = 0;

/*----------------------------------------------------------------------------
 *        Local functions
 *----------------------------------------------------------------------------*/
/** PIO pins to configure. */
static const Pin pins[] = {PIN_SPI_MISO, PIN_SPI_MOSI, PIN_SPI_SPCK, PIN_SPI_NPCS3};
/*
 * Following two functions are just for testing dont use it make your own library
 */
uint8_t read_from_reg(Spi *spi, uint8_t slave, uint8_t cs, uint8_t address) {

	uint8_t data;
	SPI_ChipSelect(spi, cs);
	SPI_Write(spi, slave, 0x80 | address);
	SPI_WriteLast(spi, slave, 0x00);
	while(!SPI_IsFinished(spi));
	data = SPI_Read(spi);
	SPI_ReleaseCS(spi);
	return data;

}

void write_to_reg(Spi *spi, uint8_t slave, uint8_t cs, uint8_t address, uint8_t data) {

		SPI_ChipSelect(spi, cs);
		SPI_Write(spi, slave, address);
		SPI_Write(spi, slave, data);
		SPI_ReleaseCS(spi);
}

typedef struct magfield {
	int16_t magx, magy, magz;
}magfield;

magfield LSM9DS1_calibmagnetometer(magfield offset) {
	if(offset.magx != 0x00) {
			write_to_reg(SPI0, SPI0_CS3, 8, OFFSET_X_REG_L_M, offset.magx);//0x18 );
			write_to_reg(SPI0, SPI0_CS3, 8, OFFSET_X_REG_H_M, offset.magx >> 8);//0xfe );
			write_to_reg(SPI0, SPI0_CS3, 8, OFFSET_Y_REG_L_M, offset.magy);//0x3a );
			write_to_reg(SPI0, SPI0_CS3, 8, OFFSET_Y_REG_H_M, offset.magy >> 8);//0x0a );
			write_to_reg(SPI0, SPI0_CS3, 8, OFFSET_Z_REG_L_M, offset.magz);//0xc6 );
			write_to_reg(SPI0, SPI0_CS3, 8, OFFSET_Z_REG_H_M, offset.magz >> 8);//0x00 );
	}
	else {
		int16_t magMin[3] = {0x0fff, 0x0fff, 0x0fff};
		int16_t magMax[3] = {0x8000, 0x8000, 0x8000};
		int16_t BiasRaw[3] = {0};
		int16_t magTemp[3] = {0};
		write_to_reg(SPI0, SPI0_CS3, 8, OFFSET_X_REG_L_M, 0x00);//0x18 );
		write_to_reg(SPI0, SPI0_CS3, 8, OFFSET_X_REG_H_M, 0x00);//0xfe );
		write_to_reg(SPI0, SPI0_CS3, 8, OFFSET_Y_REG_L_M, 0x00);//0x3a );
		write_to_reg(SPI0, SPI0_CS3, 8, OFFSET_Y_REG_H_M, 0x00);//0x0a );
		write_to_reg(SPI0, SPI0_CS3, 8, OFFSET_Z_REG_L_M, 0x00);//0xc6 );
		write_to_reg(SPI0, SPI0_CS3, 8, OFFSET_Z_REG_H_M, 0x00);//0x00 );
		printf("Starting magnetometer caliberation start moving along all axis\n\r");
		for(int i = 0; i < 250000; i++) {
			magTemp[0] =  read_from_reg(SPI0, SPI0_CS3, 8, OUT_X_L_M);
			magTemp[0] |= read_from_reg(SPI0, SPI0_CS3, 8, OUT_X_H_M) << 8;
			magTemp[1] =  read_from_reg(SPI0, SPI0_CS3, 8, OUT_Y_L_M);
			magTemp[1] |= read_from_reg(SPI0, SPI0_CS3, 8, OUT_Y_H_M) << 8;
			magTemp[2] =  read_from_reg(SPI0, SPI0_CS3, 8, OUT_Z_L_M);
			magTemp[2] |= read_from_reg(SPI0, SPI0_CS3, 8, OUT_Z_H_M) << 8;
			for(int j = 0; j < 3; j++) {
				if(magTemp[j] > magMax[j])
					magMax[j] = magTemp[j];
				if(magTemp[j] < magMin[j])
					magMin[j] = magTemp[j];
			}
		}
		for(int j = 0; j < 3; j++) {
			BiasRaw[j] = (magMin[j] + magMax[j]) / 2;
		}
		write_to_reg(SPI0, SPI0_CS3, 8, OFFSET_X_REG_L_M, BiasRaw[0]);
		write_to_reg(SPI0, SPI0_CS3, 8, OFFSET_X_REG_H_M, BiasRaw[0] >> 8);
		write_to_reg(SPI0, SPI0_CS3, 8, OFFSET_Y_REG_L_M, BiasRaw[1]);
		write_to_reg(SPI0, SPI0_CS3, 8, OFFSET_Y_REG_H_M, BiasRaw[1] >> 8);
		write_to_reg(SPI0, SPI0_CS3, 8, OFFSET_Z_REG_L_M, BiasRaw[2]);
		write_to_reg(SPI0, SPI0_CS3, 8, OFFSET_Z_REG_H_M, BiasRaw[2] >> 8);
//		offset.magx = BiasRaw[0];
//		offset.magy = BiasRaw[1];
//		offset.magz = BiasRaw[2];
		printf("During caliberation of LSM9SD1:\n");
		printf("\tMax magnetic field sense %x %x %x\n\r", magMax[0], magMax[1], magMax[2]);
		printf("\tMin magnetic field sense %x %x %x\n\r", magMin[0], magMin[1], magMin[2]);
		printf("\tAverage magnetic field sense %x %x %x\n\r", BiasRaw[0], BiasRaw[1], BiasRaw[2]);
	}
	offset.magx =  read_from_reg(SPI0, SPI0_CS3, 8, OFFSET_X_REG_L_M);
	offset.magx |= read_from_reg(SPI0, SPI0_CS3, 8, OFFSET_X_REG_H_M) << 8;
	offset.magy =  read_from_reg(SPI0, SPI0_CS3, 8, OFFSET_Y_REG_L_M);
	offset.magy |= read_from_reg(SPI0, SPI0_CS3, 8, OFFSET_Y_REG_H_M) << 8;
	offset.magz =  read_from_reg(SPI0, SPI0_CS3, 8, OFFSET_Z_REG_L_M);
	offset.magz |= read_from_reg(SPI0, SPI0_CS3, 8, OFFSET_Z_REG_H_M) << 8;
	printf("Magnetometer offset set to %x %x %x\n\r", offset.magx, offset.magy, offset.magz);
	return offset;
}

extern int main( void )
{
	uint32_t dwCnt;
	uint8_t  ucError;
	uint32_t adwBuffer[IFLASH_PAGE_SIZE / 4];
	uint32_t dwLastPageAddress;
	volatile uint32_t *pdwLastPageData;

//	uint8_t i, data = 0;
	uint16_t x = 0, y = 0, z = 0;
	uint8_t SNo[16];
	uint8_t q = 192, a = 0, b = 0, c = 0, d = 0, e = 0, f = 0;
	Async async;
	/* Disable watchdog */
	WDT_Disable(WDT);
	/* Enable I and D cache */
	SCB_EnableICache();
	SCB_EnableDCache();

	TimeTick_Configure();

	/* output examples.......................................................*/
	printf("\n\r\n\r\n\r");
	printf("EEFC Programming Example %s --\n\r", SOFTPACK_VERSION);
	printf("%s\n\r", BOARD_NAME);
	printf("-- Compiled: %s %s With %s--\n\r", __DATE__, __TIME__, COMPILER_NAME);


	/* Configure SPI pins. */
	printf("configuring SPI pins\n\r");
	PIO_Configure(pins, PIO_LISTSIZE(pins));
    printf( "\n\r-I- Configure SPI master\n\r" );
    SPI_Configure(SPI0, ID_SPI0, (SPI_MR_MSTR | SPI_MR_MODFDIS | SPI_PCS(SPI0_CS3)));

    SPI_ConfigureNPCS( SPI0,
                    SPI0_CS3,/*SPI_CSR_DLYBCT(10000) |*/
					SPI_CSR_CPOL | SPI_CSR_BITS_8_BIT|
	SPI_SCBR( spiClock, BOARD_MCK));

    /* Configure and enable interrupt on RC compare */
    NVIC_ClearPendingIRQ(SPI0_IRQn);
    NVIC_SetPriority(SPI0_IRQn ,1);
    //NVIC_EnableIRQ(SPI0_IRQn);

    //SPI_EnableIt(SPI0, SPI_IER_RDRF);
    SPI_Enable(SPI0);
	uint8_t w_data;

	printf("11\n\r");
	/*Configuring CTRL_REG values */

	uint32_t who_am_i, ctrlreg1, offsetx;
	uint32_t reg_1, reg_2, reg_3;
	SPI_ConfigureCSMode(SPI0, SPI0_CS3, 1);

	who_am_i = read_from_reg(SPI0, SPI0_CS3, 8,WHO_AM_I_M );
	who_am_i = read_from_reg(SPI0, SPI0_CS3, 8,WHO_AM_I_M );

	write_to_reg(SPI0, SPI0_CS3, 8, CTRL_REG1_M, 0xFE);

    ctrlreg1 = read_from_reg(SPI0, SPI0_CS3, 8, CTRL_REG1_M);

	write_to_reg(SPI0, SPI0_CS3, 8, CTRL_REG2_M, 0x00);
	write_to_reg(SPI0, SPI0_CS3, 8, CTRL_REG3_M, 0x00);
	write_to_reg(SPI0, SPI0_CS3, 8, CTRL_REG4_M, 0x0C);
	write_to_reg(SPI0, SPI0_CS3, 8, CTRL_REG5_M, 0x00);
//	int i, j;
	magfield offset = { .magx = 0xff0d, .magy = 0x995, .magz = 0x109 }, magneticfield;
	int16_t magMin[3] = {0x0fff, 0x0fff, 0x0fff};
	int16_t magMax[3] = {0x8000, 0x8000, 0x8000};
	int16_t BiasRaw[3] = {0};
	int16_t magTemp[3] = {0};
	double totalmagfield;

	while(1) {
		magMax[0] = magMax[1] = magMax[2] = 0x8000;
		magMin[0] = magMin[1] = magMin[2] = 0x0fff;
		write_to_reg(SPI0, SPI0_CS3, 8, OFFSET_X_REG_L_M, 0x0d);//0x67);//0x2a);
		write_to_reg(SPI0, SPI0_CS3, 8, OFFSET_X_REG_H_M, 0xff);//0xfe);//0xfe);
		write_to_reg(SPI0, SPI0_CS3, 8, OFFSET_Y_REG_L_M, 0x95);//0x44);//0xeb);
		write_to_reg(SPI0, SPI0_CS3, 8, OFFSET_Y_REG_H_M, 0x09);//0x09);//0x08);
		write_to_reg(SPI0, SPI0_CS3, 8, OFFSET_Z_REG_L_M, 0x09);//0xb7);//0xc9);
		write_to_reg(SPI0, SPI0_CS3, 8, OFFSET_Z_REG_H_M, 0x01);//0x00);//0x00);
		offsetx = read_from_reg(SPI0, SPI0_CS3, 8, OFFSET_X_REG_L_M);
		offsetx |= read_from_reg(SPI0, SPI0_CS3, 8, OFFSET_X_REG_H_M) << 8;
//		for(i = 0; i < 250000; i++) {
//			magTemp[0] =  read_from_reg(SPI0, SPI0_CS3, 8, OUT_X_L_M);
//			magTemp[0] |= read_from_reg(SPI0, SPI0_CS3, 8, OUT_X_H_M) << 8;
//			magTemp[1] =  read_from_reg(SPI0, SPI0_CS3, 8, OUT_Y_L_M);
//			magTemp[1] |= read_from_reg(SPI0, SPI0_CS3, 8, OUT_Y_H_M) << 8;
//			magTemp[2] =  read_from_reg(SPI0, SPI0_CS3, 8, OUT_Z_L_M);
//			magTemp[2] |= read_from_reg(SPI0, SPI0_CS3, 8, OUT_Z_H_M) << 8;
//			for(j = 0; j < 3; j++) {
//				if(magTemp[j] > magMax[j])
//					magMax[j] = magTemp[j];
//				if(magTemp[j] < magMin[j])
//					magMin[j] = magTemp[j];
//			}
//		}
//		for(j = 0; j < 3; j++) {
//			BiasRaw[j] = (magMin[j] + magMax[j]) / 2;
//		}
//		write_to_reg(SPI0, SPI0_CS3, 8, OFFSET_X_REG_L_M, BiasRaw[0]);
//		write_to_reg(SPI0, SPI0_CS3, 8, OFFSET_X_REG_H_M, BiasRaw[0] >> 8);
//		write_to_reg(SPI0, SPI0_CS3, 8, OFFSET_Y_REG_L_M, BiasRaw[1]);
//		write_to_reg(SPI0, SPI0_CS3, 8, OFFSET_Y_REG_H_M, BiasRaw[1] >> 8);
//		write_to_reg(SPI0, SPI0_CS3, 8, OFFSET_Z_REG_L_M, BiasRaw[2]);
//		write_to_reg(SPI0, SPI0_CS3, 8, OFFSET_Z_REG_H_M, BiasRaw[2] >> 8);
//		LSM9DS1_calibmagnetometer(offset);
		for(int i = 0; i < 10; i++) {
			magTemp[0] =  read_from_reg(SPI0, SPI0_CS3, 8, OUT_X_L_M);
			magTemp[0] |= read_from_reg(SPI0, SPI0_CS3, 8, OUT_X_H_M) << 8;
			magTemp[1] =  read_from_reg(SPI0, SPI0_CS3, 8, OUT_Y_L_M);
			magTemp[1] |= read_from_reg(SPI0, SPI0_CS3, 8, OUT_Y_H_M) << 8;
			magTemp[2] =  read_from_reg(SPI0, SPI0_CS3, 8, OUT_Z_L_M);
			magTemp[2] |= read_from_reg(SPI0, SPI0_CS3, 8, OUT_Z_H_M) << 8;
			printf("Magnetic field :  %x %x %x\n\r", magTemp[0], magTemp[1], magTemp[2]);
		}
	}
	return 0;
}
