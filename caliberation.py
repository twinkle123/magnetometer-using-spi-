import sys

if len(sys.argv) != 7:
    print('Enter valid number of arguments: <mag_x1> <mag_y1> <mag_z1> <mag_x2> <mag_y2> <mag_z2>')
    sys.exit(0)

mag_x1 = sys.argv[1].replace(",", "")
mag_y1 = sys.argv[2].replace(",", "")
mag_z1 = sys.argv[3].replace(",", "")
mag_x2 = sys.argv[4].replace(",", "")
mag_y2 = sys.argv[5].replace(",", "")
mag_z2 = sys.argv[6].replace(",", "")

if len(mag_x1) != 6:
    temp = '0x'
    for i in range(6 - len(mag_x1)):
        temp = temp + '0'
    mag_x1 = temp + mag_x1[2:]

if len(mag_y1) != 6:
    temp = '0x'
    for i in range(6 - len(mag_y1)):
        temp = temp + '0'
    mag_y1 = temp + mag_y1[2:]

if len(mag_z1) != 6:
    temp = '0x'
    for i in range(6 - len(mag_z1)):
        temp = temp + '0'
    mag_z1 = temp + mag_z1[2:]

if len(mag_x2) != 6:
    temp = '0x'
    for i in range(6 - len(mag_x2)):
        temp = temp + '0'
    mag_x2 = temp + mag_x2[2:]

if len(mag_y2) != 6:
    temp = '0x'
    for i in range(6 - len(mag_y2)):
        temp = temp + '0'
    mag_y2 = temp + mag_y2[2:]

if len(mag_z2) != 6:
    temp = '0x'
    for i in range(6 - len(mag_z2)):
        temp = temp + '0'
    mag_z2 = temp + mag_z2[2:]

print(mag_x1)
print(mag_y1)
print(mag_z1)

print(mag_x2)
print(mag_y2)
print(mag_z2)

if len(mag_x1) != 6 or len(mag_y1) != 6 or len(mag_z1) != 6:
    print('Enter the hex values in the following format: 0xhhhh')
    print('h: valid hex digit')
    sys.exit(0)

if mag_x1[2] >= '8' and (mag_x1[2] <= 'f' or mag_x1[2] <= 'F'):
    mag_x1 = -((int(mag_x1, 0) ^ 65535) + 1)
else:
    mag_x1 = int(mag_x1, 0)
if mag_y1[2] >= '8' and (mag_y1[2] <= 'f' or mag_y1[2] <= 'F'):
    mag_y1 = -((int(mag_y1, 0) ^ 65535) + 1)
else:
    mag_y1 = int(mag_y1, 0)
if mag_z1[2] >= '8' and (mag_z1[2] <= 'f' or mag_z1[2] <= 'F'):
    mag_z1 = -((int(mag_z1, 0) ^ 65535) + 1)
else:
    mag_z1 = int(mag_z1, 0)

if len(mag_x2) != 6 or len(mag_y2) != 6 or len(mag_z2) != 6:
    print('Enter the hex values in the following format: 0xhhhh')
    print('h: valid hex digit')
    sys.exit(0)

if mag_x2[2] >= '8' and (mag_x2[2] <= 'f' or mag_x2[2] <= 'F'):
    mag_x2 = -((int(mag_x2, 0) ^ 65535) + 1)
else:
    mag_x2 = int(mag_x2, 0)
if mag_y2[2] >= '8' and (mag_y2[2] <= 'f' or mag_y2[2] <= 'F'):
    mag_y2 = -((int(mag_y2, 0) ^ 65535) + 1)
else:
    mag_y2 = int(mag_y2, 0)
if mag_z2[2] >= '8' and (mag_z2[2] <= 'f' or mag_z2[2] <= 'F'):
    mag_z2 = -((int(mag_z2, 0) ^ 65535) + 1)
else:
    mag_z2 = int(mag_z2, 0)

print(mag_x1)
print(mag_y1)
print(mag_z1)

print(mag_x2)
print(mag_y2)
print(mag_z2)

avgx = (mag_x1 + mag_x2) // 2
avgy = (mag_y1 + mag_y2) // 2
avgz = (mag_z1 + mag_z2) // 2

print(str(hex(avgx)))
print(str(hex(avgy)))
print(str(hex(avgz)))
